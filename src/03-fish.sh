#!/bin/bash

# vidid - for ls_colors

VIVID_DEB_DOWNLOAD_URL=`curl -s -H "Accept: application/vnd.github.v3+json" \
    https://api.github.com/repos/sharkdp/vivid/releases/latest \
    | grep browser_download_url \
    | grep amd64.deb \
    | grep vivid_ \
    | awk -F '"' '{print $4}'`

VIVID_FILE_NAME=`echo $VIVID_DEB_DOWNLOAD_URL | awk -F '/' '{print $9}'`

wget "$VIVID_DEB_DOWNLOAD_URL" -O /tmp/$VIVID_FILE_NAME \
    && sudo dpkg -i /tmp/$VIVID_FILE_NAME

# fish

sudo apt-get -y install fzf fish

FISH_CONFIG_DIR=~/.config/fish

mkdir -p ~/.config/fish \
    && git clone git@bitbucket.org:perungrad/dotfiles-fish.git $FISH_CONFIG_DIR

# fisher plugin manager
fish -c "curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher"
