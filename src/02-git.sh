#!/bin/bash

# git, bat, delta

__DIR__=`dirname ${BASH_SOURCE[0]}`

sudo apt install -y git bat

# delta
DELTA_DEB_DOWNLOAD_URL=`curl -s -H "Accept: application/vnd.github.v3+json" \
    https://api.github.com/repos/dandavison/delta/releases/latest \
    | grep browser_download_url \
    | grep amd64.deb \
    | grep git-delta_ \
    | awk -F '"' '{print $4}'`

DELTA_FILE_NAME=`echo $DELTA_DEB_DOWNLOAD_URL | awk -F '/' '{print $9}'`

if [ -n "$DELTA_DEB_DOWNLOAD_URL" ]; then
    curl -s -L $DELTA_DEB_DOWNLOAD_URL -o /tmp/$DELTA_FILE_NAME \
        && sudo dpkg -i /tmp/$DELTA_FILE_NAME
fi

cp $__DIR__/../assets/gitconfig ~/.gitconfig
