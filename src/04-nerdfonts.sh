#!/bin/bash

FONTS_DIR=~/.local/share/fonts/NerdFonts/

mkdir -p $FONTS_DIR

curl -fLo "$FONTS_DIR/UbuntuMonoNerdFont-BoldItalic.ttf" \
    https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/UbuntuMono/Bold-Italic/UbuntuMonoNerdFont-BoldItalic.ttf?raw=true

curl -fLo "$FONTS_DIR/UbuntuMonoNerdFont-Bold.ttf" \
    https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/UbuntuMono/Bold/UbuntuMonoNerdFontMono-Bold.ttf?raw=true

curl -fLo "$FONTS_DIR/UbuntuMonoNerdFont-Italic.ttf" \
    https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/UbuntuMono/Regular-Italic/UbuntuMonoNerdFontMono-Italic.ttf?raw=true

curl -fLo "$FONTS_DIR/UbuntuMonoNerdFont-Regular.ttf" \
    https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/UbuntuMono/Regular/UbuntuMonoNerdFontMono-Regular.ttf?raw=true

fc-cache -fv
