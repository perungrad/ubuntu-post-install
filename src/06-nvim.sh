#!/bin/bash

CONF_DIR=~/.config/nvim

sudo apt-get install software-properties-common
sudo add-apt-repository ppa:neovim-ppa/unstable

sudo apt-get install -y \
    neovim \
    fzf \
    ack \
    ripgrep \
    python3-pip \
    silversearcher-ag \
    luarocks \
    golang-go

pip install pynvim
#pip install ueberzug

if [ ! -d $CONF_DIR ]; then
    mkdir -p ~/.config \
        && git clone git@bitbucket.org:perungrad/neovim.git $CONF_DIR
fi
