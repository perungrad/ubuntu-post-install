#!/bin/bash

__DIR__=`dirname ${BASH_SOURCE[0]}`
CONF_DIR=~/.config/alacritty

sudo apt-get remove rustc

sudo apt-get install -y \
    cmake \
    g++ \
    pkg-config \
    libfreetype6-dev \
    libfontconfig1-dev \
    libxcb-xfixes0-dev \
    libxkbcommon-dev \
    rustup \
    python3

rustup default stable

cargo install alacritty

if [ ! -d $CONF_DIR ]; then
    mkdir -p $CONF_DIR
    cp -r $__DIR__/../assets/alacritty/* $CONF_DIR/
    ln -s /home/$USER/.cargo/bin/alacritty ~/bin/alacritty
fi

mkdir -p ~/.config/alacritty/themes
git clone https://github.com/alacritty/alacritty-theme ~/.config/alacritty/themes

# support for emoji
mkdir -p ~/.config/fontconfig
cp -r $__DIR__/../assets/fontconfig ~/.config/
