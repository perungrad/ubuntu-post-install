#!/bin/bash

__DIR__=`dirname ${BASH_SOURCE[0]}`

grep vok_sk /usr/share/X11/xkb/rules/evdev.extras.xml > /dev/null
STATUS=$?

if [ $STATUS -eq 0 ]; then
    echo "VOK keyboard seems to be already installed"
    exit
fi

echo "Installing VOK keyboard"

ARR=(
    $"  <layout>\\n"
    $"      <configItem>\\n"
    $"        <name>vok_sk<\/name>\\n"
    $"        <shortDescription>vok_sk<\/shortDescription>\\n"
    $"        <description>Slovak-vok<\/description>\\n"
    $"        <languageList><iso639Id>slo<\/iso639Id><\/languageList>\\n"
    $"      <\/configItem>\\n"
    $"      <variantList>\\n"
    $"        <variant>\\n"
    $"          <configItem>\\n"
    $"            <name>basic<\/name>\\n"
    $"            <description>basic<\/description>\\n"
    $"          <\/configItem>\\n"
    $"        <\/variant>\\n"
    $"      <\/variantList>\\n"
    $"    <\/layout>\\n"
    $"  <\/layoutList>"
)

# no join character:
IFS=

# join lines
DATA="${ARR[*]}"

sudo cp $__DIR__/../assets/vok/vok_sk /usr/share/X11/xkb/symbols 
sudo sed -i "s/<\/layoutList>/${DATA}/" /usr/share/X11/xkb/rules/evdev.extras.xml
sudo sed -i "s/<\/layoutList>/${DATA}/" /usr/share/X11/xkb/rules/base.extras.xml

setxkbmap -layout vok_sk -variant basic
