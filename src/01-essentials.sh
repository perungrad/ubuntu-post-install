#!/bin/bash

mkdir -p ~/bin/

sudo apt-get update

sudo apt-get install -y \
    aptitude \
    mc \
    synaptic \
    pavucontrol \
    ttf-mscorefonts-installer \
    curl \
    gimp \
    keychain \
    pulseaudio-utils \
    thunar
