#!/bin/bash

CONF_DIR=~/.config/awesome

sudo apt-get install -y awesome awesome-extra arandr nitrogen blueman compton

if [ ! -d $CONF_DIR ]; then
    mkdir -p ~/.config \
        && git clone git@bitbucket.org:perungrad/dotfiles-awesome.git $CONF_DIR
fi
