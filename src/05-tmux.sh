#!/bin/bash

TMUX_CONF_DIR=~/.config/tmux
TMUX_FUNC_DIR=~/.local/share/tmux-functions

sudo apt-get install -y tmux

if [ ! -d $TMUX_CONF_DIR ]; then
    mkdir -p ~/.config \
        && git clone git@bitbucket.org:perungrad/dotfiles-tmux.git $TMUX_CONF_DIR \
        && ln -s $TMUX_CONF_DIR/tmux.conf ~/.tmux.conf
fi

if [ ! -d "$TMUX_FUNC_DIR" ]; then
    mkdir -p ~/.local/share \
        && git clone git@bitbucket.org:perungrad/tmux-functions.git $TMUX_FUNC_DIR
fi

